import { Component } from '@angular/core';

export class Villain {
  id: number;
  name: string;
}


const VILLAINS: Villain[] = [
  { id: 11, name: 'Atomic Skull' },
  { id: 12, name: 'Bizarro' },
  { id: 13, name: 'Doomsday' },
  { id: 14, name: 'Eradicator' },
  { id: 15, name: 'General Zod' },
  { id: 16, name: 'Jax-Ur' },
  { id: 17, name: 'Kryptonite Man' },
  { id: 18, name: 'Lex Luthor' }
];


@Component({
  selector: 'app-root',
  template: `
  <h1>{{title}}</h1>
  
  <h2>My Villains</h2>
  <ul class="villains">
    <li *ngFor="let v of villains" 
               [class.selected]="v === selectedVillain"
               (click)="onSelect(v)">
      <span class="badge">{{v.id}}</span> {{v.name}}
    </li>
  </ul>
  
  <div *ngIf="selectedVillain">
  <h2>{{selectedVillain.name}} details!</h2>
  <div><label>id: </label>{{selectedVillain.id}}</div>
  <div>
    <label>name: </label>
    <input [(ngModel)]="selectedVillain.name" placeholder="name">
  </div>
  </div>
  `,
  styles: [`
  .selected {
    background-color: #CFD8DC !important;
    color: white;
  }
  .villains {
    margin: 0 0 2em 0;
    list-style-type: none;
    padding: 0;
    width: 15em;
  }
  .villains li {
    cursor: pointer;
    position: relative;
    left: 0;
    background-color: #EEE;
    margin: .5em;
    padding: .3em 0;
    height: 1.6em;
    border-radius: 4px;
  }
  .villains li.selected:hover {
    background-color: #BBD8DC !important;
    color: white;
  }
  .villains li:hover {
    color: #607D8B;
    background-color: #DDD;
    left: .1em;
  }
  .villains .text {
    position: relative;
    top: -3px;
  }
  .villains .badge {
    display: inline-block;
    font-size: small;
    color: white;
    padding: 0.8em 0.7em 0 0.7em;
    background-color: #607D8B;
    line-height: 1em;
    position: relative;
    left: -1px;
    top: -4px;
    height: 1.8em;
    margin-right: .8em;
    border-radius: 4px 0 0 4px;
  }
`]
})
export class AppComponent {
  title = 'Tour of Villains';
  // villain: Villain = {
  //   id: 1,
  //   name: 'Lex Luthor'
  // };
  selectedVillain: Villain;
  villains = VILLAINS;

  onSelect(villain: Villain): void {
    this.selectedVillain = villain;
  }
}
